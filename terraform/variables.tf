variable "akey" {
  description = "AWS access key"
}

variable "skey" {
  description = "AWS secret key"
}
