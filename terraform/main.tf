terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {} 
}

# Configure and downloading plugins for aws
provider "aws" {
  access_key = var.akey
  secret_key = var.skey
  region     = "us-west-2"
}

resource "aws_instance" "Gitlab-Runner" {
  ami           = "ami-0f1a5f5ada0e7da53"
  instance_type = "t2.micro"
  key_name      = "ec2-rojeeme"

  tags = {
    Name = "Gitlab-Runner"
  }
}

